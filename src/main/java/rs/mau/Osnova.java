package rs.mau;

import aterm.pure.ATermApplImpl;
import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.ontology.*;
import com.hp.hpl.jena.rdf.model.*;
import com.hp.hpl.jena.reasoner.Reasoner;
import com.hp.hpl.jena.reasoner.ReasonerRegistry;
import com.hp.hpl.jena.reasoner.ValidityReport;
import com.hp.hpl.jena.util.FileManager;
import com.hp.hpl.jena.vocabulary.DC;
import com.hp.hpl.jena.vocabulary.ReasonerVocabulary;
import de.fuberlin.wiwiss.d2rq.jena.GraphD2RQ;
import de.fuberlin.wiwiss.d2rq.jena.ModelD2RQ;
import de.fuberlin.wiwiss.d2rq.map.Mapping;
import de.fuberlin.wiwiss.d2rq.parser.MapParser;
import org.mindswap.pellet.ABox;
import org.mindswap.pellet.KnowledgeBase;
import org.mindswap.pellet.PelletOptions;
import org.mindswap.pellet.jena.PelletInfGraph;
import org.mindswap.pellet.jena.PelletReasonerFactory;

import java.io.InputStream;
import java.util.Iterator;
import java.util.List;


/**
 *
 * http://stackoverflow.com/questions/24550670/mapping-database-with-d2rq
 */
public class Osnova {


    public static void main(String[] args) {
        Zakljucivanje k = new Zakljucivanje();
        //k.doReason();
        //String s = k.prvoPravilo( "31", "7" );
        //System.out.println(s);

        k.rezonZaStudenta(31);
        //double[] doubles = Queries.trajanjeSesija(k, "31");
        //System.out.println(doubles[0] + " sdas " + doubles[1]);
        //k.doAll();

        //Queries.checkUserId("3");

        //k.trajanjeSesija();

        //k.stampajModel();
        //k.napraviSparqlUpit("3","10");



       //rs.mau.Osnova m= new rs.mau.Osnova();
       //m.TrecaMetoda(5);
       // m.petaMetoda();

       //m.sestaMetoda();

        //m.mainTry();
       // m.nesto();





        //- sa jenom probati kako radi rezonovanje u pelletu
        //
    }


    public static void printIterator(Iterator<?> i, String header) {
        System.out.println(header);
        for(int c = 0; c < header.length(); c++)
            System.out.print("=");
        System.out.println();

        if(i.hasNext()) {
            while (i.hasNext())
                System.out.println( i.next() );
        }
        else
            System.out.println("<EMPTY>");

        System.out.println();
    }
}
