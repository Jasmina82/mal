package rs.mau;

import com.hp.hpl.jena.datatypes.xsd.XSDDateTime;
import com.hp.hpl.jena.ontology.*;
import com.hp.hpl.jena.query.*;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.*;
import com.hp.hpl.jena.reasoner.Reasoner;
import com.hp.hpl.jena.util.FileManager;
import de.fuberlin.wiwiss.d2rq.jena.ModelD2RQ;
import org.mindswap.pellet.KnowledgeBase;
import org.mindswap.pellet.jena.PelletInfGraph;
import org.mindswap.pellet.jena.PelletReasonerFactory;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;


//trebalo bi podeliti na 3 klase Zakljucivanje, Misc i sparql upitpi

public class Zakljucivanje {

    public static  String idFsq; //starting fsq

    public static final String ONTOLOGY_FILE = "mau.owl"; //ont1.owl
    public static final String MAPPING_TTL = "rezonMapiranje.ttl"; //eventualno resurs

        //https://github.com/tiberiu-b/diet4elders/blob/4a1e9828c0bb85141f03f67d992e473af745eea6/src/main/java/org/licenta/d4elders/ontology/FoodProviderOntology.java
    public static final String ONT_POLICY_RDF = "asdas";
    public static final String putanja= "http://www.mau.rs/mau.owl";
    public static XSDDateTime currentDateTime;

    public static final String  prefixi =
            "PREFIX mau: <http://www.mau.rs/mau.owl#>" +
            "PREFIX owl: <http://www.w3.org/2002/07/owl#>" +
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>" +
            "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> " ;

    public static Double gradeVal;

    public Model d2rData;
    public OntModel ontModel;
    public Model model;

    public Zakljucivanje(){
        currentDateTime = new XSDDateTime(Calendar.getInstance()); //uzima trenutni UTC

        try {
            //TODO treba iskljuciti warninge da se upisuju u warn; samo errors
            System.setOut(new PrintStream(new FileOutputStream("./out.txt")));
            System.setErr(new PrintStream(new FileOutputStream("./warn.txt")));


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }


    public void stampajModel() {

        InfModel infModel = initMainOntModel();

        ubaciD2DataModel();
        ontModel.write(System.out);
    }

    private KnowledgeBase getKnowledgeBase(InfModel infModel) {

        PelletInfGraph a = ((PelletInfGraph) infModel.getGraph() );
        KnowledgeBase kb = a.getKB();
        kb.prepare();

        kb.realize();
        kb.classify();

        return kb;
    }

    private InfModel getInfModel() {
        Model ontData = getOntologyModel();
        Reasoner reasoner = PelletReasonerFactory.theInstance().create();
        InfModel infModel = ModelFactory.createInfModel(reasoner, ontData); //ontData
        //reasoner.setDerivationLogging(true);
        return infModel;
    }

    private InfModel initMainOntModel() {
        InfModel infModel = getInfModel();
        OntModelSpec spec = new OntModelSpec(OntModelSpec.OWL_MEM); //OWL_MEM
        ontModel = ModelFactory.createOntologyModel(spec, infModel);

        return infModel;
    }

    protected void ubaciD2DataModel() {
        try {
            model = FileManager.get().loadModel(MAPPING_TTL);
            d2rData = new ModelD2RQ(model, "http://www.mau.rs/resource/");

            ontModel.add(d2rData);

        } catch (Exception e) {
            System.out.println( d2rData );
            e.printStackTrace();
        }

    }



    public Individual insertIndividualWithoutStyle(Object numerickiParametar,
                                                   OntModel miniM,
                                                   String user,
                                                   String nazivKlase,
                                                   String nazivPropertija,
                                                   String tempIndividual,
                                                   String resurs) {


        //todo numericki parametar verovartno treba pretvriti u double ili int
        //uraditi debug videti kako ga prepoznaje; videti koji upiti zavrsavaju ovde

        OntClass viewedUa = miniM.getOntClass(  nazivKlase );
        Individual indNov = miniM.createIndividual(   tempIndividual , viewedUa );



        DatatypeProperty propertyWNumber = miniM.createDatatypeProperty(  nazivPropertija  );

        Literal propertyNumberValue = miniM.createTypedLiteral( numerickiParametar ); //+0.0
        indNov.setPropertyValue( propertyWNumber , propertyNumberValue );

        if(resurs != "") {
            //da li je za sve viewsResource?
            DatatypeProperty propertyViewsResource = miniM.createDatatypeProperty(putanja + "#viewsResource");

            Literal viewsResourceValue = miniM.createTypedLiteral(resurs);
            indNov.setPropertyValue(propertyViewsResource, viewsResourceValue);
        }

        ObjectProperty relatedToUser = miniM.createObjectProperty(putanja + "#relatedToUser");
        ObjectProperty objectProperty1 = miniM.createObjectProperty(  "http://www.mau.rs/resource/user/"+user );
        indNov.setPropertyValue(relatedToUser,objectProperty1);

        return indNov;

    }



    public void insertIndividualStyle(    int brojPregleda,
                                           OntModel miniM,
                                           String user,
                                           String nazivKlase,
                                           String nazivPropertija,
                                           String tempIndividual,
                                           String putanjaPrivremneInstanceLOS ) {


        Individual indNov  = insertIndividualWithoutStyle(brojPregleda,miniM, user,nazivKlase,nazivPropertija, tempIndividual, "");

        //*1 todo proveriti da li treba za sve  putanjaPrivremeneInstanceLOS da se doda users
        ObjectProperty relatedToLOStyle = miniM.createObjectProperty(putanja + "#relatedToLOS");
        ObjectProperty individualLOS = miniM.createObjectProperty( putanjaPrivremneInstanceLOS ); //*1
        indNov.setPropertyValue(relatedToLOStyle,individualLOS);
    }



    public void insertIndStyleViewsResource(long brojPregleda,
                                            OntModel miniM,
                                            String user,
                                            String nazivKlase,
                                            String nazivPropertija,
                                            String tempIndividual,
                                            String resurs ) {


        Individual indNov  = insertIndividualWithoutStyle(brojPregleda,miniM, user,nazivKlase,nazivPropertija, tempIndividual, resurs);


        ObjectProperty viewsResource = miniM.createObjectProperty(putanja + "#viewsResource");
        ObjectProperty individualViewsResource = miniM.createObjectProperty( resurs );
        indNov.setPropertyValue(viewsResource,individualViewsResource);
    }


    public Model getOntologyModel() {



        OntDocumentManager dm = new OntDocumentManager(ONT_POLICY_RDF);
        OntModelSpec modelSpec = PelletReasonerFactory.THE_SPEC;
        modelSpec.setDocumentManager(dm);
        OntModel ontModelWithImport = ModelFactory.createOntologyModel(modelSpec);

        //SPARQL upit

        //http://www.mau.rs:2020/snorql/

        //TTL
        Model model1= ontModelWithImport.read(FileManager.get().open(ONTOLOGY_FILE),null,"RDF/XML");

        //RDF/XML
        //"RDF/XML-ABBREV"
        //Model model;

        try {
            InputStream open =
                    //new URL("http://www.mau.rs/mau.owl").openStream();
                    FileManager.get().open(ONTOLOGY_FILE);
            model = ontModelWithImport.read( open, "");
        }
        catch (Exception e){
            model = ontModelWithImport;
        }
        return model;
    }




    public Literal izvrsiBrojUpit(String sparqlQueryString) {
        String endpoint = "http://www.mau.rs:2020/sparql"; //?output=json
        //"http://dbpedia.org/sparql";
        Query query = QueryFactory.create(sparqlQueryString);
        // System.out.println( query );
        QueryExecution qexec = QueryExecutionFactory.sparqlService(endpoint , query);

        //System.out.println( qexec );

        ResultSet results = qexec.execSelect();
        //ResultSetFormatter.out(System.out, results, query);
        //System.out.println( ); //.getResultVars()

        Literal res = null;
        QuerySolution next = results.next();

        if( ! next.toString().isEmpty() ) {
            res = next.getLiteral("?broj");
        }
        qexec.close() ;

        return res;
    }


    public Object dbQuery(String query, String tip) {


        List<Object> rezul = new ArrayList<>();
        java.sql.ResultSet rs = null;
        boolean res =false ;

        Connection conn = null;
        try
        {
            //jdbc:mysql://192.168.15.25:3306/mydb
            String url = "jdbc:mysql://212.47.242.226:3306/moodle";
            Class.forName ("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection (url,"remote","remote sifra");
           // System.out.println ("Database connection established");


            java.sql.Statement stmt = conn.createStatement();
            //String query = "SELECT * FROM mdl_block_mau WHERE 1";
            if(tip =="select"){
                rs = stmt.executeQuery( query );
                rezul = resultSetToArrayList(rs);
            }

            else if(tip == "insert"){
                res = stmt.execute(query);
            }


        }
        catch (Exception e)
        {
            e.printStackTrace();

        }
        finally
        {
            if (conn != null)
            {
                try
                {
                    conn.close ();
                  //  System.out.println ("Database connection terminated");
                }
                catch (Exception e) { /* ignore close errors */ }
            }
        }

        if(tip == "insert"){return res;}
        return rezul;
                //"uspesno povezano sa bazom";
    }

    public List resultSetToArrayList( java.sql.ResultSet rs) throws SQLException{
        ResultSetMetaData md = rs.getMetaData();
        int columns = md.getColumnCount();
        ArrayList list = new ArrayList();

        while (rs.next()){
            HashMap row = new HashMap(columns);
            for(int i=1; i<=columns; ++i){
                row.put(md.getColumnName(i),rs.getObject(i));
            }
            list.add(row);
        }

        return list;
    }


    public boolean upisiUBazu(String userId, String acre, String vive, String sein, String segl) {

        String q_t = String.format( "INSERT INTO mdl_block_mau (id, userid,acre,vive,sein, segl, tip_ocene)"
                + " VALUES (null, '%d','%s','%s','%s','%s','rezon')",
                Integer.parseInt(userId),acre, vive, sein, segl);
        boolean res = (boolean) dbQuery(q_t,"insert");
        return res;
    }


    // mozda: za poboljsanje uzeti samo one ocene koje su u intervalu od zadnjih nedelju dana
    public String doAll(){

        String upit =
                prefixi +
                " SELECT DISTINCT ?id (count(  ?s ) AS ?broj)  WHERE { " + //
                "  ?s rdf:type	mau:User . " +
                "  ?ocena rdf:type mau:Grades . " +
                " ?ocena mau:relatedToUser ?s . " +
                " ?s mau:hasId ?id .  " +
                " } " +
                " group by ?id  LIMIT 10"; //

        String endpoint = "http://www.mau.rs:2020/sparql"; //?output=json

        Query q = QueryFactory.create( upit );
        QueryExecution qexec = QueryExecutionFactory.sparqlService(endpoint , q);

        ResultSet results = qexec.execSelect();


        while( results.hasNext() ){
            QuerySolution unos = results.next();
            int brojOcena = unos.getLiteral("?broj").getInt();

            if(brojOcena >= 1)
            {
                int idStudenta = unos.getLiteral("?id").getInt();

                rezonZaStudenta(idStudenta);

                System.out.println( "broj ocena za studenta " + idStudenta +" : "+ brojOcena );

            }

        }


        return  "izvrseno";
    }


    /** Pravilo
     *
     *
     * Pravilo:
     * User(?u) ^ FelderSilvermanQuestionary(?fsq) ^ FelderSilvermanQuestionary(?res)
     * ^ ViewedUA(?nov) ^ TotalGrades(?tg) ^ SequentialLO(?le)
     ^ seqGlo(?fsq, "seq") +
     ^ relatedToUser(?fsq, ?u) +
     ^ relatedToUser(?nov, ?u)
     ^ relatedToUser(?tg, ?u)
     ^ relatedToLOS(?nov, ?le)
     ^ hasGradesType(?res, "rezonTemp")
     ^ gradeValue(?tg, ?gradeVal)
     ^ swrlb:greaterThan(?gradeVal, 0.5)
     ^ numberOfViews(?nov, ?brojPregleda)
     ^ swrlb:greaterThan(?brojPregleda, 1.0) // sledeci korak, birmo onaj stil gde ima vise poseta
     * @param idStudenta link to user
     * @return owl individual containing results
     */
    public boolean rezonZaStudenta(int idStudenta) {

        String user ="" + idStudenta, lesson="";


        boolean st = Queries.checkUserId(user);
        //todo  da li postoji ocena za studenta; ocena je preduslov za rezonovanje
        if(!st){
            return false;
        }

        initMainOntModel();
        ubaciD2DataModel();

        //mini model
        InfModel infModelMini = getInfModel();
        OntModelSpec specMini = new OntModelSpec(OntModelSpec.OWL_MEM);
        OntModel miniModel = ModelFactory.createOntologyModel(specMini, infModelMini);

      
        String[] dimenzije =
                {"sequential", "global",
                "visual", "verbal",
                "sensitive","intuitive",
                "active","reflexive"
                };


        for (int i=0; i<=7; i++){ //svaki drugi stil se uzima
            if(i%2==0) {
                // zakljuci jednu dimenziju
                String temp_stil = dimenzije[i];
                ubaciStilUcenja(user, miniModel, temp_stil);
            }

        }

        /* lesson time i trajanje sesija*/
        double[] sesije = Queries.trajanjeSesija(this, user);
        double trajanjeSesija = sesije[0];
        double ukupnaSesija = (sesije.length ==2)? sesije[1] : 0;

        /*int vremeSvihLekcija = Queries.vremeProvedenoLekcija(this, user,"2") ;
        //double koeficijent = (ukupnaSesija==0)? (vremeSvihLekcija/ukupnaSesija)*//**10*//* : 0;
        //ovde dobijemo neki procenat ~= 0.043 >< 0.5 <>0.75

        Queries.ubaciLessonTime(this, vremeSvihLekcija, ukupnaSesija,miniModel, user);*/

        //not related to style, related to user
        ubaciIndividuale(user, miniModel,"2");
        //TODO COURSE id - znaci da ce imati vise ocena u jednom trenutku? za svaki kurs



        Queries.insertFsqRes(ontModel, miniModel, idStudenta ); //rezultujuci FSQ

        idFsq = Queries.lastFelderSilvermanQForUser(user);


        // ako nema prethodnog fsq-a rezonovanje nije moguce; nije uradjen init test
        if(idFsq == null || idFsq.equalsIgnoreCase("-1") )
            return false;

        /* individual from starting model - user and starting fsq */
        cloneIndividuals(user, miniModel, idFsq);


        //reasoning based on mini model
        KnowledgeBase kb = getKnowledgeBase(infModelMini);


        Individual result = miniModel.getIndividual(putanja + "#fsqRes"+idStudenta);
        Property pr = miniModel.getProperty( putanja + "#timeCreated" );
        String tempTime= result.getPropertyValue(pr).asLiteral().getLexicalForm();


       // listajSveIndividuale(miniModel);

        Individual rezultat = miniModel.getIndividual("http://www.mau.rs/mau.owl#fsqRes"+idStudenta);
                //(putanja + "fsqRes");
        doWithResults(user, infModelMini, miniModel, rezultat);


        //uzeti sve kurseve koje pohadja user

        String[] niz = Queries.allCoursesForUser(this,user);



        for (int i = 0; i < niz.length ; i++) {
            ubaciParametreIzvestaj(user, trajanjeSesija, ukupnaSesija , niz[i] ); //id kursa
        }


       // miniModel.write(System.out);


        return true;
    }



    private void ubaciParametreIzvestaj(String user, double prosecnaSesija, double ukSesija, String courseId) {

        //double[] sesije = Queries.trajanjeSesija(this, user);
        double trajanjeSesija = prosecnaSesija;
                //sesije[0];
        double ukupnaSesija = ukSesija;
                //(sesije.length ==2)? sesije[1] : 0;

        int brojPoslatihPorukaUser = Queries.brojPoslatihPoruka(this, user);
        int brojLoginovaUser = Queries.brojLogin(this, user);
        int brojLogoutaUser = Queries.brojLogout(this, user);

        int brojPoseta = Queries.brojPoseta(this, user, courseId);
        int brojSubmmitovaWorkshop = Queries.brojSubmissionaZaStil(this, user, "mau:WorkshopSubmissions", courseId);
        int brojSubmmitovaAssign = Queries.brojSubmissionaZaStil(this, user, "mau:AssignSubmission", courseId);
        int brojUploadZaStil = Queries.brojUploadZaStil(this, user , courseId);
        int brojEvaluatedZaStil = Queries.brojEvaluatedZaStil(this, user , courseId);
        int brojForumPostova = Queries.brojForumPostova(this, user, courseId );
        int answerdLessonPages = Queries.brojAnswerdLessonPages(this, user, courseId );
        int brojAttemptsQuiz = Queries.brojAttemptsZaStil(this, user, "quiz", courseId );
        int brojAttemptsLesson = Queries.brojAttemptsZaStil(this, user, "lesson", courseId );


        int vremeLekcija = Queries.vremeProvedenoLekcija(this, user, courseId); //TODO lekcija
        int vremeQuiz = Queries.vremeProvedenoQuiz(this, user, courseId );


        gradeVal = Queries.izracunajTotalGrade(this, user, courseId );



        String upit = String.format(
                "INSERT INTO semanticki_izvestaj "
                +"(id, user, felder_silverman_id,course, grade_avg, sesion_avg,  msgSent, time_lesson, time_quiz, " +
                "logins, logouts, views, submit_workshop, submit_assign, uploads, evaluated, posts , answered," +
                " attempts_quiz, attempts_lesson, reportDate)"
                + " VALUES (null, '%d','%d','%d','%10s','%10s','%d','%d','%d'," +
                " '%d','%d','%d','%d','%d','%d','%d','%d','%d'," +
                " '%d', '%d', now())",
                Integer.parseInt(user),Integer.parseInt(idFsq), Integer.parseInt(courseId)  , gradeVal, trajanjeSesija+"",  brojPoslatihPorukaUser, vremeLekcija, vremeQuiz,
                brojLoginovaUser, brojLogoutaUser,brojPoseta, brojSubmmitovaWorkshop, brojSubmmitovaAssign, brojUploadZaStil,
                brojEvaluatedZaStil, brojForumPostova, answerdLessonPages,brojAttemptsQuiz,brojAttemptsLesson);

        Object rez =  dbQuery(upit,"insert");

        System.out.println("User : " +user);
        System.out.println("Kurs : " +courseId);
        System.out.println("Prosecna ocena : " + (gradeVal * 100 ) + " %" );
        System.out.println("Prosecna sesija u minutima : " + (trajanjeSesija/60.0 )  );
        System.out.println("Ukupna sesija u minutima : " + (ukupnaSesija/60.0 ) );
        System.out.println("Broj poslatih poruka: " +brojPoslatihPorukaUser);
        System.out.println("Broj loginova : " +brojLoginovaUser);
        System.out.println("Broj logout-a : " +brojLogoutaUser);
        System.out.println("Broj poseta svim resursima: " +brojPoseta);
        System.out.println("Broj submittovanih radionica: " +brojSubmmitovaWorkshop);
        System.out.println("Broj submittovanih zadataka : " +brojSubmmitovaAssign);
        System.out.println("Broj uploadovanih fajlova: " +brojUploadZaStil);
        System.out.println("Broj ocenjivanja drugih studenata: " +brojEvaluatedZaStil);
        System.out.println("Broj forum postova: " +brojForumPostova);
        System.out.println("Broj odgovora u lekcijima: " +answerdLessonPages);
        System.out.println("Broj pohadjanja kvizova : " +brojAttemptsQuiz);
        System.out.println("Broj pohadjanja lekcija: " +brojAttemptsLesson);
        System.out.println("Vreme provedeno na lekcijama u minutima : " + (vremeLekcija /60.0 ) );
        System.out.println("Vreme provedeno na kvizovima u minutima : " + (vremeQuiz /60.0 ) );
        System.out.println("\n");
    }

    private void ubaciIndividuale(String user, OntModel miniModel, String courseId ) {

        int brojResursaAssign = Queries.brojPojavljivanja(this,courseId,"Assign");
        int brojResursaGlossasry = Queries.brojPojavljivanja(this,courseId,"Glossary");
        int brojResursaLesson = Queries.brojPojavljivanja(this,courseId,"Lesson");
        int brojResursaQuiz = Queries.brojPojavljivanja(this,courseId,"Quiz");
        int brojResursaWiki = Queries.brojPojavljivanja(this,courseId,"Wiki");
        int brojResursaWorkshop = Queries.brojPojavljivanja(this,courseId,"Workshop");
        /*fajlovi nemaju direktnu vezu sa kursom*/
        int brojResursaVideoF = Queries.brojPojavljivanjaFile(this,courseId,"VideoF");
        int brojResursaTextF = Queries.brojPojavljivanjaFile(this,courseId,"TextF");
        int brojResursaMultimediaF = Queries.brojPojavljivanjaFile(this,courseId,"MultimediaF");

        Queries.ubaciBorderValues(this, miniModel,brojResursaAssign,courseId,"Assign");
        Queries.ubaciBorderValues(this, miniModel,brojResursaGlossasry,courseId,"Glossary");
        Queries.ubaciBorderValues(this, miniModel,brojResursaLesson,courseId,"Lesson");
        Queries.ubaciBorderValues(this, miniModel,brojResursaQuiz,courseId,"Quiz");
        Queries.ubaciBorderValues(this, miniModel,brojResursaVideoF,courseId,"VideoF");
        Queries.ubaciBorderValues(this, miniModel,brojResursaTextF,courseId,"TextF");
        Queries.ubaciBorderValues(this, miniModel,brojResursaMultimediaF,courseId,"MultimediaF");
        Queries.ubaciBorderValues(this, miniModel,brojResursaWiki,courseId,"Wiki");
        Queries.ubaciBorderValues(this, miniModel,brojResursaWorkshop,courseId,"Workshop");


        int brojPosetaMultimedia = Queries.noViewedFile(this, user,"MultimediaF",courseId );
        Queries.ubaciViewedUARes(this,brojPosetaMultimedia,miniModel, user,"multimediaF");
        int brojPosetaTextF = Queries.noViewedFile(this, user,"TextF",courseId );
        Queries.ubaciViewedUARes(this,brojPosetaTextF,miniModel, user,"textF");
        int brojPosetaPictureF = Queries.noViewedFile(this, user,"PictureF",courseId );
        Queries.ubaciViewedUARes(this,brojPosetaPictureF,miniModel, user,"pictureF");
        long quizView = Queries.noViewedResource(this, user, "quiz",courseId );
        Queries.ubaciViewedUARes(this,quizView,miniModel, user,"quiz");
        long forumView = Queries.noViewedResource(this, user, "forum",courseId );
        Queries.ubaciViewedUARes(this,forumView,miniModel, user,"forum");
        long workshopView = Queries.noViewedResource(this, user, "workshop",courseId );
        Queries.ubaciViewedUARes(this,workshopView,miniModel, user,"workshop");
        long glossaryView = Queries.noViewedResource(this, user, "glossary",courseId );
        Queries.ubaciViewedUARes(this,glossaryView,miniModel, user,"glossary");
        long scormView = Queries.noViewedResource(this, user, "scorm",courseId );
        Queries.ubaciViewedUARes(this,scormView,miniModel, user,"scorm");
        long lessonView = Queries.noViewedResource(this, user, "lesson",courseId );
        Queries.ubaciViewedUARes(this,lessonView,miniModel, user,"lesson");
        long assignView = Queries.noViewedResource(this, user, "assign",courseId );
        Queries.ubaciViewedUARes(this,assignView,miniModel, user,"assign");



        int ukupnePosete = Queries.brojPoseta(this, user, courseId );
        Queries.ubaciViewedUA(this, ukupnePosete, miniModel, user, "" ); //ViewedUA za jedan stil

        int brojForumPostova = Queries.brojForumPostova(this, user, courseId);
        Queries.ubaciPostsUA(this, brojForumPostova, miniModel, user, "" ); //PostedUA za jedan stil


        gradeVal = 0.0;
        gradeVal = Queries.izracunajTotalGrade(this, user, courseId );
        Queries.ubaciTotalGrade(ontModel, gradeVal, miniModel, user); //TotalGrades for user



    }

    private void doWithResults(String user, InfModel infModelMini, OntModel miniModel, Individual rezultat) {
        KnowledgeBase kb;
        Individual result, initFsq ;// stampajPropertijeIndividuala(rezultat);

        //1. vrednosti sa inicijalnim pretpostavkama za stilove
        String rezSeqGl = vrednostPropertija(miniModel, rezultat, "seqGlo");
        String rezVisVrb = vrednostPropertija(miniModel, rezultat, "visVrb");
        String rezSenInt  = vrednostPropertija(miniModel, rezultat, "senInt");
        String rezActRef  = vrednostPropertija(miniModel, rezultat, "actRef");

        boolean pravilo1 = (rezSeqGl == null);
        boolean pravilo2 = (rezVisVrb == null);
        boolean pravilo3 = (rezSenInt == null);
        boolean pravilo4 = (rezActRef == null);

        //pravilo izvrseno, nema te dimenzije -> probaj sa suprotnom
        if( pravilo1 ){            ubaciStilUcenja(user, miniModel, "global");               }
        if( pravilo2 ){                ubaciStilUcenja(user, miniModel, "verbal");        }
        if( pravilo3 ){            ubaciStilUcenja(user, miniModel, "intuitive");        }
        if( pravilo4 ){             ubaciStilUcenja(user, miniModel, "reflexive");        }



        //2.ako neko pravilo nije vratilo odgovarajuci rezultat
        //URADI ZAKLJUCIVAVNJE PONOVO sa suprotnom dimenzijom (npr. ac==null -> uzmi za ref)
        if( pravilo1 || pravilo2 || pravilo3 || pravilo4){
            kb = getKnowledgeBase(infModelMini); //ponovno zakljucivanje
            result = miniModel.getIndividual(putanja + "#fsqRes"+user);
            rezSeqGl = vrednostPropertija(miniModel, rezultat, "seqGlo");
            rezVisVrb = vrednostPropertija(miniModel, rezultat, "visVrb");
            rezSenInt  = vrednostPropertija(miniModel, rezultat, "senInt");
            rezActRef  = vrednostPropertija(miniModel, rezultat, "actRef");
        }
        initFsq = miniModel.getIndividual("http://www.mau.rs/resource/FelderSilvermanQuestionary/"+idFsq);




        //3. ako je vrednost posle dva prolaska kroz pravila == null, nisu ispunjni uslovi ni za jedno pravilo-> prikazuje se init vrednost
        if(rezSeqGl  ==null){  rezSeqGl = vrednostPropertija(miniModel, initFsq, "seqGlo");}
        if(rezVisVrb  ==null){  rezVisVrb = vrednostPropertija(miniModel, initFsq, "visVrb");}
        if(rezSenInt  ==null){  rezSenInt = vrednostPropertija(miniModel, initFsq, "senInt");}
        if(rezActRef  ==null){  rezActRef = vrednostPropertija(miniModel, initFsq, "actRef");}


        //miniModel.write(System.out);

        System.out.println("rezultujuci stil ucenja: ");


        upisiUBazu(user,rezActRef,rezVisVrb,rezSenInt,rezSeqGl);

        System.out.println(rezActRef);
        System.out.println(rezSenInt);
        System.out.println(rezVisVrb);
        System.out.println(rezSeqGl);
    }

    private void cloneIndividuals(String user, OntModel miniModel, String idFsq) {
        Individual userIndividual = ontModel.getIndividual("http://www.mau.rs/resource/user/" + user);
        Individual fsqIndividual = ontModel.getIndividual("http://www.mau.rs/resource/FelderSilvermanQuestionary/" + idFsq);
        //Individual fsqRezInvdividual = ontModel.getIndividual(putanja + "#fsqRes");

        Misc.cloneIndividualToNewModel(userIndividual, miniModel);
        Misc.cloneIndividualToNewModel(fsqIndividual, miniModel);
        //cloneIndividualToNewModel(fsqRezInvdividual, miniModel);
    }

    private String vrednostPropertija(OntModel miniModel, Individual rezultat, String properti) {
        String  tempRezSt = null ;

        if(rezultat==null || properti.isEmpty())
            return  tempRezSt;
        Property tempProp = miniModel.getProperty( putanja + "#" + properti );
        RDFNode tempRez = rezultat.getPropertyValue(tempProp);



        if( tempRez != null)
            tempRezSt =  tempRez.asLiteral().getLexicalForm();

        return tempRezSt;
    }


    public void ubaciStilUcenja(String user, OntModel miniModel, String stil_temp) {

        ubaciLo(stil_temp, miniModel); //SequentialLO

      /*  brojPoseta = Queries.brojPoseta(this, user,"", stil_temp);
        brojForumPostova = Queries.brojForumPostova(this, user, "", stil_temp);

        Queries.ubaciViewedUA(this, brojPoseta, miniModel, user, stil_temp ); //ViewedUA za jedan stil
        Queries.ubaciPostsUA(this, brojForumPostova, miniModel, user, stil_temp ); //PostedUA za jedan stil*/

       /* //ovde ide sve sa stilom i studentom
       brojSubmmitova = Queries.brojSubmissionaZaStil(this, user, "", stil_temp);
        brojUploadZaStil = Queries.brojUploadZaStil(this, user, course_temp, stil_temp);
        brojEvaluatedZaStil = Queries.brojEvaluatedZaStil(this, user, course_temp, stil_temp);

        answerdLessonPages = Queries.brojAnswerdLessonPages(this, user, course_temp, stil_temp);
        brojAttemptsZaStil = Queries.brojAttemptsZaStil(this, user, course_temp, stil_temp);*/

    }


    public void ubaciLo(String stil, OntModel miniM) {
        String stilIzabran = getStyleName(stil);

        if(miniM == null)
            miniM = ontModel;

        OntClass stilizovanaKlasa = miniM.getOntClass( putanja  + "#" + stilIzabran );
        Individual indNov = miniM.createIndividual( putanja + "#" + stilIzabran + "inst", stilizovanaKlasa );

        //DatatypeProperty numberOfViewes = miniM.createDatatypeProperty( putanja + "#numberOfViews" );


    }

    public String getStyleName(String stil) {
        String stilIzabran = "";



        switch ( stil ){
            case "active": stilIzabran = "ActiveLO"; break;
            case "reflexive": stilIzabran = "ReflexiveLO"; break;
            case "sensitive": stilIzabran = "SensitiveLO"; break;
            case "intuitive": stilIzabran = "IntuitiveLO"; break;
            case "sequential": stilIzabran = "SequentialLO"; break;
            case "global": stilIzabran = "GlobalLO"; break;
            case "visual": stilIzabran = "VisualLO"; break;
            case "verbal": stilIzabran = "VerbalLO"; break;

        }
        return stilIzabran;
    }

}
