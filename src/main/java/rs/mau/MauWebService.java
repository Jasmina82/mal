package rs.mau;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;



/*Ovaj web servis treba da prima id studenta i da za njega vraca
stil ucenja kao json objekat.

Vraca stil ucenja tako sto se povezuje sa jenom i poziva odredjenu proceduru u jeni.
Procedura u jena-i, izvrsava rezonovanje i vraca rezultat

*/

@Path("/mau")
public class MauWebService {




    /**
     * A GET /mau/{id} request should return a entry from the LearningStyle subsystem
     * @param id the unique identifier of a
     * @return a JSON representation of the new entry or 404
     */
    @GET
    @Path("/user/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLs(@PathParam("id") int id, @QueryParam("parametar") String param) {

        //prima id korisnika, vraca json objekat sa stilovima ucenja

        //napravi instancu  stila ucenja, pozovi izvrsavanje

        String la = "neuspesno" ;
        boolean rez = false;

        Zakljucivanje k = new Zakljucivanje();

        rez = k.rezonZaStudenta(id);

        if(rez)
            la ="uspesno";

        return Response.ok(la , MediaType.APPLICATION_JSON).build();

    }




    /**
     * A GET /mau/all request should return a entry from the ToDo list
     * @return a JSON representation of the new entry or 404
     */
    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response doAll() {
        String la ;

        Zakljucivanje k = new Zakljucivanje();

        la = k.doAll();

        return Response.ok(la , MediaType.APPLICATION_JSON).build();



        //return Response.status(Status.NOT_FOUND).build();
    }


    @GET
    @Path("/test")
    @Produces(MediaType.APPLICATION_JSON)
    public Response testPutanje() {
        String la = "test putanja" ;



        return Response.ok(la , MediaType.APPLICATION_JSON).build();



        //return Response.status(Status.NOT_FOUND).build();
    }

}
