package rs.mau;


import com.hp.hpl.jena.ontology.*;
import com.hp.hpl.jena.query.*;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.vocabulary.XSD;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public class Queries {


    public static boolean checkUserId( String user) {
        String sparqlQueryString =
                Zakljucivanje.prefixi +
                    " SELECT   distinct *  WHERE { " +

                    "?s rdf:type	mau:User . " +
                    "?s mau:hasId " +  user + " . "+
                    "?s mau:firstName ?name ." +

                    "?ag rdf:type	mau:Grades . " +
                    "?ag mau:relatedToLOwithGrades ?grad .  " +
                    "?ag mau:gradeValue ?val .  " +
                    "?grad mau:gradeValue ?maxG .  " +
                    "?ag mau:relatedToUser ?s ." +
                    "}  limit 10 ";

        String endpoint = "http://www.mau.rs:2020/sparql"; //?output=json
        Query query = QueryFactory.create(sparqlQueryString);

        QueryExecution qexec = QueryExecutionFactory.sparqlService(endpoint , query);
        ResultSet results = qexec.execSelect();


        System.out.println(results);
        try {
            QuerySolution querySolution = results.next();
            Literal student = querySolution.getLiteral("?name"); //nema studenta
            Literal ocena = querySolution.getLiteral("?val"); //nema ocene
            //System.out.println(res.getString());
        }
        catch (NoSuchElementException e){
            System.out.println("nema takvog studenta"); //nema ocene
            return false;
        }

        qexec.close() ;
        return true;
    }

    public static String[] allCoursesForUser(Zakljucivanje zakljucivanje, String user) {

        String[] rez = new String[1];
        String upit =
                " SELECT ue.id as id, ue.userid as user ,e.courseId as kurs " +
                " FROM `mdl_user_enrolments` ue inner join mdl_enrol e on ue.`enrolid`=e.id " +
                " WHERE ue.userid = "+ user ;

        ArrayList lista = (ArrayList) zakljucivanje.dbQuery(upit, "select");

        if(lista.size() > 0){
            rez = new String[lista.size()];
        }
        else {
            rez[0] = "";
        }


        for (int i = 0; i <lista.size() ; i++) {
           rez[i] = (Long) ((HashMap)lista.get(i)).get("courseid") + "";
        }



        return rez;
    }


    public static int brojPoseta(Zakljucivanje zakljucivanje, String userId, String courseId) {

        //String stilIzabran = zakljucivanje.getStyleName( stil );

        String sparqlQueryString =
                Zakljucivanje.prefixi +
                " SELECT (count(  ?s ) AS ?broj)   WHERE { " +
                "?s mau:logsCourse  ?c . "+
                "?c mau:hasId "+courseId+" ." +
                "?s  mau:relatedToUser	<http://www.mau.rs:2020/resource/user/" + userId + "> . " +
                "?s mau:action 'viewed'^^xsd:string ." +
                "?s mau:relatedTo ?v . " +
                //"?v mau:loStyle     mau:"+stilIzabran+" . " +
                        "?v rdf:type ?type ." +
                "FILTER (?type not in (mau:LOwithGrades, mau:Files)) "  +
                "}  limit 10 ";

        Literal literal = zakljucivanje.izvrsiBrojUpit(sparqlQueryString);

        int anInt = 0;
        if(literal!=null)
            anInt = literal.getInt();
        return anInt;
    }


    public static int vremeProvedenoLekcija(Zakljucivanje zakljucivanje, String userId , String courseId) {



        String sparqlQueryString =
                Zakljucivanje.prefixi +
                        "SELECT  ( sum(?time)   as ?broj) WHERE { " +
                        "?s ?p mau:LessonTimer . " +
                        "?s mau:timeSpent ?time. " +
                        "?s mau:relatedToUser	<http://www.mau.rs:2020/resource/user/"+userId +">  . " +
                        "?s mau:relatedToLesson ?l . " +
                        "?l mau:relatedToCourse ?c ." +
                        "?c mau:hasId "+courseId+" ." +
                        "} " +
                        "LIMIT 10" ;
        Literal literal = zakljucivanje.izvrsiBrojUpit(sparqlQueryString);

        int anInt = 0;
        if(literal!=null)
            anInt = literal.getInt();
        return anInt;
    }


    public static long noViewedResource(Zakljucivanje zakljucivanje, String userId, String resource, String courseId) {


        String query = "SELECT count(*) as broj FROM `mdl_logstore_standard_log` "
                    +"WHERE `action` LIKE 'viewed' AND `userid` = " + userId
                + " AND courseid ='" + courseId + "'"
                + " AND objecttable IN ('" + resource + "')"
                +" AND FROM_UNIXTIME(`timecreated`) >= DATE_SUB(NOW(), INTERVAL "+  /*50*/ 7 + " DAY) ORDER BY `id` DESC";

        //System.out.println(query);
        Object rez =  zakljucivanje.dbQuery(query, "select");
        HashMap hm = (HashMap) ((ArrayList) rez).get(0);

        return (long) hm.get("broj");
    }


    public static int brojPojavljivanja(Zakljucivanje zakljucivanje, String courseId, String tipResursa){
        String query =
                Zakljucivanje.prefixi +
                        "SELECT (count(?s) as ?broj)  WHERE { " +
                        "?s ?p mau:"+tipResursa+" . " +
                        "?s mau:relatedToCourse ?c . " +
                        "?c mau:hasId "+courseId+" . " +
                         "} " +
                        "LIMIT 10 " ;
        Literal literal = zakljucivanje.izvrsiBrojUpit(query);

        int anInt = 0;
        if(literal!=null)
            anInt = literal.getInt();
        return anInt;



    }

    public static int brojPojavljivanjaFile(Zakljucivanje zakljucivanje, String courseId, String tipFajla){
        String query =
                Zakljucivanje.prefixi +
                        "SELECT (count(?s) as ?broj)  WHERE { " +
                        "?s ?p mau:Logs . "+
                        "?s mau:logsCourse ?c . "+
                        "?c mau:hasId "+courseId+" . "+
                        "?s mau:relatedToFile ?f . "+
                        "?f  rdf:type	mau:"+tipFajla+" . "+
                        "} " +
                        "LIMIT 10 " ;
        Literal literal = zakljucivanje.izvrsiBrojUpit(query);

        int anInt = 0;
        if(literal!=null)
            anInt = literal.getInt();
        return anInt;


    }

    public static int noViewedFile(Zakljucivanje zakljucivanje, String userId, String tipFajla, String courseId){
        String query =
                Zakljucivanje.prefixi +
                "SELECT  (count(  ?s ) AS ?broj)  WHERE { "+
                "	?s rdf:type	mau:Logs . "+
                "  ?s mau:logsCourse ?c . "+
                " ?c mau:hasId "+courseId+" . "+
                " ?s mau:relatedToUser <http://www.mau.rs:2020/resource/user/" + userId + "> . " +
                "	?s mau:relatedToFile ?f . "+
                "	?f rdf:type mau:" + tipFajla + " . "+ //MultimediaF
                "} "+
                " LIMIT 10";
        Literal literal = zakljucivanje.izvrsiBrojUpit(query);

        int anInt = 0;
        if(literal!=null)
            anInt = literal.getInt();
        return anInt;
    }


        public static int brojUploadZaStil(Zakljucivanje zakljucivanje, String userId, String courseId/*, String stil*/) {


            String sparqlQueryString =
                    Zakljucivanje.prefixi +
                            " SELECT (count(  ?s ) AS ?broj)   WHERE { " +
                            "?s mau:logsCourse  ?c . "+
                            "?c mau:hasId "+courseId+" . "+
                            "?s  mau:relatedToUser	<http://www.mau.rs:2020/resource/user/" + userId + "> . " +
                            "?s mau:action 'uploaded'^^xsd:string ." +
                            "?s mau:relatedTo ?v . " +
                            //"?v mau:loStyle     mau:"+stilIzabran+" . " +
                            "?v rdf:type ?type ." +
                            "FILTER (?type not in (mau:LOwithGrades, mau:Files)) "  +
                            "}  limit 10 ";

            //workshop i assign; ~= submitted

            Literal literal = zakljucivanje.izvrsiBrojUpit(sparqlQueryString);

            int anInt = 0;
            if(literal!=null)
                anInt = literal.getInt();
            return anInt;
        }




    static int brojPoslatihPoruka(Zakljucivanje zakljucivanje, String userId) {

        String sparqlQueryString =
                Zakljucivanje.prefixi +
                    "SELECT  (count(  ?s ) AS ?broj) WHERE { " +
                    "	?s mau:relatedToMessage ?m . " +
                    "	?m mau:userFrom	<http://www.mau.rs:2020/resource/user/"+userId+"> . " +
                    "} " +
                    "LIMIT 10 " ;


        Literal literal = zakljucivanje.izvrsiBrojUpit(sparqlQueryString);

        int anInt = 0;
        if(literal!=null)
            anInt = literal.getInt();
        return anInt;

    }

    public static int brojEvaluatedZaStil(Zakljucivanje zakljucivanje, String userId, String courseId/*, String stil*/) {

        String sparqlQueryString =
                Zakljucivanje.prefixi +
                        "SELECT  (count(  ?s ) AS ?broj) WHERE { " +
                        "?s mau:action 'evaluated'^^xsd:string . " +
                        "?s mau:relatedTo ?p . " +
                        "?s mau:logsCourse  ?c . "+
                        "?c mau:hasId "+courseId+" . "+
                        "?p mau:relatedToUser 	<http://www.mau.rs:2020/resource/user/"+userId+"> . " +
                        "?p mau:relatedToWorkshop ?ws . " +
                        //"?ws mau:loStyle mau:" + stilIzabran+" . " +
                       // "?ws mau:relatedToCourse <http://www.mau.rs:2020/resource/course/"+courseId+"> . " +
                        "} " +
                        "LIMIT 10 ";

        Literal literal = zakljucivanje.izvrsiBrojUpit(sparqlQueryString);

        int anInt = 0;
        if(literal!=null)
            anInt = literal.getInt();
        return anInt;
    }

    public static int brojSubmissionaZaStil(Zakljucivanje zakljucivanje, String userId, String param, String courseId) {


        String sparqlQueryString =
                Zakljucivanje.prefixi +

        " SELECT   (count(  ?ws ) AS ?broj)  WHERE { " +

                "?u rdf:type mau:User . " +
                "?ws mau:relatedToUser ?u . " +
                "?u mau:hasId "+ userId + " . " +
                "?ws  mau:relatedToResource ?objekat . " +
                "?ws rdf:type " + param+" . " +
                //"?objekat mau:loStyle ?tip1 . " +
                "?objekat mau:relatedToCourse 	?c . " +
                "?c mau:hasId "+courseId+" . "+
                // "filter (?tip1 in (mau:"+stilIzabran+") ) " + // mau:

                "}  limit 100  " ;

        Literal literal = zakljucivanje.izvrsiBrojUpit(sparqlQueryString);

        int anInt = 0;
        if(literal!=null)
            anInt = literal.getInt();
        return anInt;
    }

    public static int brojForumPostova(Zakljucivanje zakljucivanje, String userId, String courseId) {


        String sparqlQueryString =
                Zakljucivanje.prefixi +

                " SELECT  (count(  ?s ) AS ?broj) WHERE { " +
                "?s mau:action 'uploaded'^^xsd:string . " +
                "?s mau:objecttable 'forum_posts'^^xsd:string . " +
                "?s mau:logsCourse  ?c . "+
                "?c mau:hasId "+courseId+" . "+

                "?s mau:relatedTo ?f . " +
                "?f mau:postedByUser <http://www.mau.rs:2020/resource/user/"+userId+"> . " +
                "?f mau:relatedToForumDiscussions ?d . " +
                "?d mau:relatedToForum ?foru .  " +
               // "?foru mau:loStyle mau:" + stilIzabran + " . " +
                "?foru mau:relatedToCourse 	?c . " +
                "} " +
                "LIMIT 10 "  ;




        Literal literal = zakljucivanje.izvrsiBrojUpit(sparqlQueryString);

        int anInt = 0;
        if(literal!=null)
            anInt = literal.getInt();
        return anInt;
    }

    public static int brojAnswerdLessonPages(Zakljucivanje zakljucivanje, String userId, String courseId) {


        String sparqlQueryString =
                Zakljucivanje.prefixi +

                "SELECT distinct (count(  ?s ) AS ?broj)  WHERE { " +
                "?s mau:action 'answered'^^xsd:string . " +
                "?s mau:logsCourse  ?c . "+
                "?c mau:hasId "+courseId+" . "+
                "?s mau:relatedTo ?rel . " +
                "?s mau:relatedToUser <http://www.mau.rs:2020/resource/user/"+userId+"> . " +
                "?rel mau:relatedToLesson ?l . " +
                //"?l mau:loStyle mau:"+stilIzabran+" . " +
                "?l  mau:relatedToCourse ?c.  " +
                "}  " +
                "LIMIT 10 " ;




        Literal literal = zakljucivanje.izvrsiBrojUpit(sparqlQueryString);

        int anInt = 0;
        if(literal!=null)
            anInt = literal.getInt();
        return anInt;
    }

    public static int brojAttemptsZaStil(Zakljucivanje zakljucivanje, String userId, String param, String courseId) {
        String tip = "";
        String veza = "";

         if(param == "quiz"){
             veza = "mau:relatedToQuiz";
             tip = "mau:QuizAttempts";
         }
        else if(param == "lesson"){
             veza = "mau:relatedToLesson";
             tip = "mau:LessonAttempts";
         }

        //uzmi u obzir samo posete koje su nastale u poslednjih nedelju dana; ili u poslednjih dve nedelje
        String sparqlQueryString =
                Zakljucivanje.prefixi +

                        "SELECT  (count(  ?ws ) AS ?broj)   WHERE { " +

                        "?u rdf:type mau:User . " +
                        "?u mau:hasId "+userId+" . " +
                        "?ws mau:relatedToUser ?u . " +
                        "?ws rdf:type " + tip + " . " +
                        "?ws  " + veza +" ?objekat . " +
                        "?objekat mau:relatedToCourse 	?c . " +
                        "?c mau:hasId "+courseId+" . "+

                        "}  limit 100  " ;


        Literal literal = zakljucivanje.izvrsiBrojUpit(sparqlQueryString);

        int anInt = 0;
        if(literal!=null)
            anInt = literal.getInt();
        return anInt;
    }

    public static int brojLogin(Zakljucivanje zakljucivanje, String userId) {

        //ovde mozda ubaciti switch sa osam stilova ucenja



        //uzmi u obzir samo posete koje su nastale u poslednjih nedelju dana; ili u poslednjih dve nedelje
        String sparqlQueryString =
                Zakljucivanje.prefixi +

                        "SELECT  (count(  ?s ) AS ?broj)   WHERE { " +
                        "?s mau:action 'loggedin'^^xsd:string . "+
                        "?s mau:relatedToUser    <http://www.mau.rs:2020/resource/user/"+userId+"> . " +

                        "}  limit 10  " ;





        Literal literal = zakljucivanje.izvrsiBrojUpit(sparqlQueryString);

        int anInt = 0;
        if(literal!=null)
            anInt = literal.getInt();
        return anInt;
    }

    public static int brojLogout(Zakljucivanje zakljucivanje, String userId) {

        //ovde mozda ubaciti switch sa osam stilova ucenja



        //uzmi u obzir samo posete koje su nastale u poslednjih nedelju dana; ili u poslednjih dve nedelje
        String sparqlQueryString =
                Zakljucivanje.prefixi +

                "SELECT  (count(  ?s ) AS ?broj)   WHERE { " +
                "?s mau:action 'loggedout'^^xsd:string . "+
                "?s mau:relatedToUser    <http://www.mau.rs:2020/resource/user/"+userId+"> . " +

                "}  limit 10  " ;


        Literal literal = zakljucivanje.izvrsiBrojUpit(sparqlQueryString);

        int anInt = 0;
        if(literal!=null)
            anInt = literal.getInt();
        return anInt;
    }

    public static String lastFelderSilvermanQForUser(String s) {
        //todo course?
        String upitnik =
                Zakljucivanje.prefixi +
                " SELECT DISTINCT ?fsq ?fsq_id WHERE {"+
                "  ?fsq mau:relatedToUser	<http://www.mau.rs:2020/resource/user/"+s+"> ."+
                "  ?fsq rdf:type	mau:FelderSilvermanQuestionary . "+
                "  ?fsq mau:hasId ?fsq_id"+
                " } "+
                " order by DESC(?fsq_id)"+
                " LIMIT 1";

        String endpoint = "http://www.mau.rs:2020/sparql"; //?output=json

        Query q = QueryFactory.create( upitnik );
        QueryExecution qexec = QueryExecutionFactory.sparqlService(endpoint , q);

        ResultSet results = qexec.execSelect();

        int id_fsq_a = -1;

        if( results.hasNext() ){
            id_fsq_a  = results.next().getLiteral("?fsq_id").getInt();
        }

        return  id_fsq_a+"";

    }



    public Double prosekAssign(Zakljucivanje zakljucivanje, String userId, String interval, String courseId){

        String upit =
                "SELECT avg( ag.grade /a.grade ) as ocena " +
                        "FROM `mdl_assign_grades` as ag inner join mdl_assign " +
                        "as a on ag.`assignment`= a.id " +
                        "WHERE ag.`userid` ="+userId+" AND " +
                        " a.course = "+ courseId +" and " +
                        "FROM_UNIXTIME(ag.`timecreated`) >= DATE_SUB(NOW(), INTERVAL "+ interval +" DAY)";

        //System.out.println(query);
        Object rez =  zakljucivanje.dbQuery(upit, "select");
        HashMap hm = (HashMap) ((ArrayList) rez).get(0);

        if(hm.get("ocena") == null) return 0.0;
        return Double.parseDouble(hm.get("ocena").toString())  ;


    }



    public Double prosekLesson(Zakljucivanje zakljucivanje, String userId, String interval, String courseId){
    //todo course
        String upit =
                "SELECT avg( lg.grade /l.grade ) as ocena "+
                "FROM `mdl_lesson_grades` as lg "+
                "INNER JOIN mdl_lesson as l "+
                "ON lg.`lessonid`= l.id "+
                "WHERE lg.`userid` ="+userId+" "+
                "and l.`course` = "+ courseId +" " +
                "AND FROM_UNIXTIME(lg.`completed`) >= DATE_SUB(NOW(), INTERVAL "+interval+" DAY)";

        //System.out.println(query);
        Object rez =  zakljucivanje.dbQuery(upit, "select");
        HashMap hm = (HashMap) ((ArrayList) rez).get(0);

        if(hm.get("ocena") == null) return 0.0;
        return Double.parseDouble(hm.get("ocena").toString()) ;
    }


    public Double prosekQuiz(Zakljucivanje zakljucivanje, String userId, String interval, String courseId){
        //todo course
        String upit =
                "SELECT avg( qg.grade /q.grade ) as ocena " +
                        "FROM `mdl_quiz_grades` as qg " +
                        "INNER JOIN mdl_quiz as q " +
                        "ON qg.`quiz`= q.id " +
                        "WHERE qg.`userid` ="+userId+"  " +
                        "and q.`course` = "+ courseId +" " +
                        "AND FROM_UNIXTIME(qg.`timemodified`) >= DATE_SUB(NOW(), INTERVAL "+interval+" DAY)" ;

        //System.out.println(query);
        Object rez =  zakljucivanje.dbQuery(upit, "select");
        HashMap hm = (HashMap) ((ArrayList) rez).get(0);

        if(hm.get("ocena") == null) return 0.0;
        return Double.parseDouble(hm.get("ocena").toString()) ;
    }

    public Double prosekScorm(Zakljucivanje zakljucivanje, String userId, String interval, String courseId){
        //todo course
        String upit =
                "SELECT avg( st.value /s.maxGrade ) as ocena "+
                        "FROM `mdl_scorm_scoes_track` as st "+
                        "INNER JOIN mdl_scorm as s "+
                        "ON st.`scormid`= s.id "+
                        "WHERE st.`userid` ="+userId+" "+
                        "and s.`course` = "+ courseId +" " +
                        "AND FROM_UNIXTIME(st.`timemodified`) >= DATE_SUB(NOW(), INTERVAL "+interval+" DAY) "+
                        "AND `element` LIKE 'cmi.score.raw'" ;

        //System.out.println(query);
        Object rez =  zakljucivanje.dbQuery(upit, "select");
        HashMap hm = (HashMap) ((ArrayList) rez).get(0);

        if(hm.get("ocena") == null) return 0.0;
        return (double) hm.get("ocena");
    }

    public Double prosekWorkshop(Zakljucivanje zakljucivanje, String userId, String interval, String courseId){
        //todo course id
        String upit =

                "SELECT avg( wa.`gradinggrade` /(w.grade + w.`gradinggrade` ) ) as ocena "+
                "FROM `mdl_workshop_aggregations` wa "+
                "INNER JOIN mdl_workshop w "+
                "ON wa.`workshopid` = w.id "+
                "WHERE `userid` = "+userId+"  " +
                "and w.`course` = "+ courseId +" " +
                "AND FROM_UNIXTIME(wa.`timegraded`) >= DATE_SUB(NOW(), INTERVAL "+interval+" DAY)" ;

        //System.out.println(query);
        Object rez =  zakljucivanje.dbQuery(upit, "select");
        HashMap hm = (HashMap) ((ArrayList) rez).get(0);

        if(hm.get("ocena") == null) return 0.0;
        return Double.parseDouble(hm.get("ocena").toString()) ;
    }



    public static Double izracunajTotalGrade(Zakljucivanje zakljucivanje, String user, String courseId) {


        Queries q = new Queries();

        List<Double> niz = new ArrayList<>();

        String interval = "150";

        Double prosekAssign = q.prosekAssign(zakljucivanje, user, interval, courseId);
        niz.add(prosekAssign);

        Double prosekLesson = q.prosekLesson(zakljucivanje, user, interval, courseId);
        niz.add(prosekLesson);
        Double prosekQuiz = q.prosekQuiz(zakljucivanje, user, interval, courseId);
        niz.add(prosekQuiz);
        Double prosekScorm = q.prosekScorm(zakljucivanje, user, interval, courseId);
        niz.add(prosekScorm);
        Double prosekWorkshop = q.prosekWorkshop(zakljucivanje, user, interval, courseId);
        niz.add(prosekWorkshop);

        Double suma = 0.0;

        int brojac=0;
        for (int i = 0; i <niz.size() ; i++) {
            if(niz.get(i)==0.0) continue;
            suma+=niz.get(i);
            brojac++;
        }
        if(brojac == 0 ) brojac = 1;

        return suma/brojac;
    }

    public static Double izracunajTotalGradeOld(Zakljucivanje zakljucivanje, String user) {
        String upit =
                Zakljucivanje.prefixi +
                " SELECT  ( (AVG (?val/?maxG)  ) as  ?broj ) "+
                " WHERE { " +

                " ?ag rdf:type	mau:Grades . " +
                " ?ag mau:relatedToLOwithGrades ?grad . " +
                " ?ag mau:gradeValue ?val . "  +
                " ?grad mau:gradeValue ?maxG . " +
                " ?ag mau:relatedToUser <http://www.mau.rs:2020/resource/user/"+ user +"> . " +
                " FILTER(isNumeric(?val)) " +
                " } " +
                " limit 100 ";

        Literal res = zakljucivanje.izvrsiBrojUpit(upit);

        return res.getDouble();
    }






    /*
    * mini model insert
    * */


    public static void insertFsqRes(OntModel ontModel, OntModel whichModel, int idStudenta) {

        if(whichModel == null)
            whichModel = ontModel;

        OntClass fsq = whichModel.getOntClass(Zakljucivanje.putanja  + "#FelderSilvermanQuestionary");
        Individual indFsqRes = whichModel.createIndividual(Zakljucivanje.putanja + "#fsqRes"+idStudenta, fsq);

        DatatypeProperty timeCreated = whichModel.createDatatypeProperty(Zakljucivanje.putanja + "#timeCreated");
        timeCreated.addDomain(indFsqRes);
        //timeCreated.addRange(dateTime);

        DatatypeProperty hasGradesType = whichModel.createDatatypeProperty(Zakljucivanje.putanja + "#hasGradesType");
        hasGradesType.addDomain(indFsqRes);
        hasGradesType.addRange(XSD.xstring);

        Literal valueTimeCreated = whichModel.createTypedLiteral(Zakljucivanje.currentDateTime);
        indFsqRes.setPropertyValue(timeCreated, valueTimeCreated);

        Literal valueHasGradesType = whichModel.createTypedLiteral("rezonTemp");
        indFsqRes.setPropertyValue(hasGradesType, valueHasGradesType);
    }

    //, String user, String lesson
    public static void ubaciViewedUA(Zakljucivanje zakljucivanje, int brojPregleda, OntModel miniM, String user, String nazivLOS) {

        if(miniM == null)
            miniM = zakljucivanje.ontModel;

        String nazivKlase = Zakljucivanje.putanja  + "#ViewedUA";
        String nazivPropertija = Zakljucivanje.putanja  + "#numberOfViews";
        //"related to reosurce" = "quiz"
        String tempIndividual = Zakljucivanje.putanja  + "#vua"+nazivLOS + user ;
        String putanjaPrivremeneInstanceLOS = Zakljucivanje.putanja + "#" + zakljucivanje.getStyleName(nazivLOS) + "inst";

        zakljucivanje.insertIndividualStyle(brojPregleda, miniM, user, nazivKlase, nazivPropertija, tempIndividual, putanjaPrivremeneInstanceLOS);
    }

    //, String user, String lesson
    public static void ubaciViewedUARes(Zakljucivanje zakljucivanje, long brojPregleda, OntModel miniM, String user, String resurs) {

        if(miniM == null)
            miniM = zakljucivanje.ontModel;

        String nazivKlase = Zakljucivanje.putanja  + "#ViewedUA";
        String nazivPropertija = Zakljucivanje.putanja  + "#numberOfViews";
        //String viewsResource = Zakljucivanje.putanja  + "#viewsResource";
        //"related to reosurce" = "quiz"
        String tempIndividual = Zakljucivanje.putanja  + "#vua"+resurs + user ; //objekat individual
        //String putanjaPrivremeneInstanceLOS = Zakljucivanje.putanja + "#" + resurs + user+ "inst";
        //ActiveLo instanca

        Individual indNov  = zakljucivanje.insertIndividualWithoutStyle(brojPregleda,miniM, user,nazivKlase,nazivPropertija, tempIndividual, resurs);

    }


    public static void ubaciTotalGrade(OntModel ontModel, Double gradeVal, OntModel miniM, String user) {
        if(miniM == null)
            miniM = ontModel;

        OntClass tg = miniM.getOntClass( Zakljucivanje.putanja  + "#TotalGrades" );
        Individual indNov = miniM.createIndividual( Zakljucivanje.putanja + "#tg"+user, tg );

        DatatypeProperty gVal = miniM.createDatatypeProperty( Zakljucivanje.putanja + "#gradeValue" );


        //gradeVal = gradeVal.
        BigDecimal vred = BigDecimal.valueOf(  gradeVal +0.0   );
        Literal grade = miniM.createTypedLiteral( vred );  //String.format( "%.2f", vred  )

        //grade.get
        indNov.setPropertyValue(gVal , grade);


        ObjectProperty relatedToUser = miniM.createObjectProperty(Zakljucivanje.putanja + "#relatedToUser");
        ObjectProperty objectProperty1 = miniM.createObjectProperty( "http://www.mau.rs/resource/user/"+user);
        indNov.setPropertyValue(relatedToUser,objectProperty1);
    }



    static void ubaciAttemptsUA(Zakljucivanje zakljucivanje, int brojAttemptsZaStil, OntModel miniModel, String user, String stil_temp) {

        if(miniModel == null)
            miniModel = zakljucivanje.ontModel;

        String nazivKlase = Zakljucivanje.putanja  + "#AttemptUA";
        String nazivPropertija = Zakljucivanje.putanja  + "#numberOfAttempts";
        String tempIndividual = Zakljucivanje.putanja  + "#atua" + stil_temp + user ;
        String putanjaPrivremneInstanceLOS = Zakljucivanje.putanja + "#" + zakljucivanje.getStyleName(stil_temp) + "inst";

        zakljucivanje.insertIndividualStyle(brojAttemptsZaStil, miniModel, user, nazivKlase, nazivPropertija, tempIndividual, putanjaPrivremneInstanceLOS);


    }

    static void ubaciAnswerdUA(Zakljucivanje zakljucivanje, int answerdLessonPages, OntModel miniModel, String user, String stil_temp) {
        if(miniModel == null)
            miniModel = zakljucivanje.ontModel;

        String nazivKlase = Zakljucivanje.putanja  + "#AnswerdUA";
        String nazivPropertija = Zakljucivanje.putanja  + "#numberOfAnswers";
        String tempIndividual = Zakljucivanje.putanja  + "#aua"+stil_temp + user ;
        String putanjaPrivremneInstanceLOS = Zakljucivanje.putanja + "#" + zakljucivanje.getStyleName(stil_temp) + "inst";

        zakljucivanje.insertIndividualStyle(answerdLessonPages, miniModel, user, nazivKlase, nazivPropertija, tempIndividual, putanjaPrivremneInstanceLOS);

    }

    public static void ubaciPostsUA(Zakljucivanje zakljucivanje, int brojForumPostova, OntModel miniModel, String user, String stil_temp) {
        if(miniModel == null)
            miniModel = zakljucivanje.ontModel;

        String nazivKlase = Zakljucivanje.putanja  + "#PostedUA";
        String nazivPropertija = Zakljucivanje.putanja  + "#numberOfPosts";
        String tempIndividual = Zakljucivanje.putanja  + "#pua" + stil_temp + user ;
        String putanjaPrivremneInstanceLOS = Zakljucivanje.putanja + "#" + zakljucivanje.getStyleName(stil_temp) + "inst";

        zakljucivanje.insertIndividualStyle(brojForumPostova, miniModel, user, nazivKlase, nazivPropertija, tempIndividual, putanjaPrivremneInstanceLOS);

    }

    public static void ubaciEvaluatedUA(Zakljucivanje zakljucivanje, int brojEvaluatedZaStil, OntModel miniModel, String user, String stil_temp) {
        if(miniModel == null)
            miniModel = zakljucivanje.ontModel;

        String nazivKlase = Zakljucivanje.putanja  + "#EvaluatedUA";
        String nazivPropertija = Zakljucivanje.putanja  + "#numberOfEvaluation";
        String tempIndividual = Zakljucivanje.putanja  + "#eua"+stil_temp + user ;
        String putanjaPrivremneInstanceLOS = Zakljucivanje.putanja + "#" + zakljucivanje.getStyleName(stil_temp) + "inst";

        zakljucivanje.insertIndividualStyle(brojEvaluatedZaStil, miniModel, user, nazivKlase, nazivPropertija, tempIndividual, putanjaPrivremneInstanceLOS);

    }


    public static void ubaciUploadedUA(Zakljucivanje zakljucivanje, int brojUploadZaStil, OntModel miniModel, String user, String stil_temp) {

        if(miniModel == null)
            miniModel = zakljucivanje.ontModel;

        String nazivKlase = Zakljucivanje.putanja  + "#UploadedUA";
        String nazivPropertija = Zakljucivanje.putanja  + "#numberOfUploads";
        String tempIndividual = Zakljucivanje.putanja  + "#uua" + stil_temp + user ;
        String putanjaPrivremneInstanceLOS = Zakljucivanje.putanja + "#" + zakljucivanje.getStyleName(stil_temp) + "inst";
        // todo konzistentna putanja za StilLO

        zakljucivanje.insertIndividualStyle(brojUploadZaStil, miniModel, user, nazivKlase, nazivPropertija, tempIndividual, putanjaPrivremneInstanceLOS);
    }




    public static void ubaciLoginUA(Zakljucivanje zakljucivanje, int brojLogin, OntModel miniModel, String user) {
        if(miniModel == null)
            miniModel = zakljucivanje.ontModel;

        String nazivKlase = Zakljucivanje.putanja  + "#LoggedinUA";
        String nazivPropertija = Zakljucivanje.putanja  + "#numberOfLogins";
        String tempIndividual = Zakljucivanje.putanja  + "#lua"+user;
        //String putanjaPrivremneInstanceLOS = Zakljucivanje.putanja + "#" + user + "inst";

        zakljucivanje.insertIndividualWithoutStyle(brojLogin ,  miniModel, user, nazivKlase, nazivPropertija, tempIndividual, "");


    }

    public static void ubaciLogoutUA(Zakljucivanje zakljucivanje, int brojLogout, OntModel miniModel, String user) {
        if(miniModel == null)
            miniModel = zakljucivanje.ontModel;

        String nazivKlase = Zakljucivanje.putanja  + "#LoggedoutUA";
        String nazivPropertija = Zakljucivanje.putanja  + "#numberOfLogouts";
        String tempIndividual = Zakljucivanje.putanja  + "#loua"+user;
       // String putanjaPrivremneInstanceLOS = Zakljucivanje.putanja + "#" + user + "inst";

        zakljucivanje.insertIndividualWithoutStyle(brojLogout ,  miniModel, user, nazivKlase, nazivPropertija, tempIndividual, "");
    }

    public static void ubaciTrajanjeSesija(Zakljucivanje zakljucivanje, double trajanjeSesija, OntModel miniModel, String user) {
        if(miniModel == null)
            miniModel = zakljucivanje.ontModel;

        String nazivKlase = Zakljucivanje.putanja  + "#SessionUA";
        String nazivPropertija = Zakljucivanje.putanja  + "#averageDurationSession";
        String tempIndividual = Zakljucivanje.putanja  + "#ads"+user;
        //String putanjaPrivremneInstanceLOS = Zakljucivanje.putanja + "#" + user + "inst";


        //ne moze ova metoda, koja povezuje propertije za stil, mora da postoji posebna
        zakljucivanje.insertIndividualWithoutStyle(trajanjeSesija ,  miniModel, user, nazivKlase, nazivPropertija, tempIndividual, "");

    }

    /*ovde imamo proseke za sve usere? */
    public static double[] trajanjeSesija(Zakljucivanje zakljucivanje, String userId){
        double[] rez = new double[1];

        List<Unos> izlogovani = new ArrayList<Unos>();
        List<Unos> ulogovani = new ArrayList<Unos>();

        String upit= "select * from `moodle`.`mdl_logstore_standard_log` as l1 where l1.action in ('loggedout', 'loggedin' )  and `userid` =" + userId;
        List<HashMap> resultSet =(ArrayList<HashMap>) zakljucivanje.dbQuery( upit , "select");

        try {
            for (HashMap un: resultSet){
                long id = (long)un.get("id");//resultSet.getInt("id");
                long userid = (long)un.get("userid");//resultSet.getInt("userid");
                long timecreated = (long) un.get("timecreated");
                String action = (String) un.get("action");

                Unos tempUnos = new Unos();
                tempUnos.id=id;
                tempUnos.userid=userid;
                tempUnos.timecreated=timecreated;
                tempUnos.action=action;

                if(action.equals( "loggedout" )){
                    izlogovani.add(tempUnos);
                }
                else {
                    ulogovani.add(tempUnos);
                }

            }


            List<Unos> collect = izlogovani.stream().map(unos -> {
                long id_temp   = unos.id;

                Unos maximalni = new Unos() ;

               for ( Unos la : ulogovani ){
                   if(la.id > maximalni.id && la.id < id_temp && unos.userid ==la.userid )
                   {
                       maximalni = la;
                   }

                   /*if(maximalni.userid==0)
                       return null;*/
                   //ulogovani.remove(maximalni);

               }
                maximalni.timeStart = maximalni.timecreated;
                maximalni.timeEnd = unos.timecreated;
                maximalni.trajanje = unos.timecreated - maximalni.timecreated;

                if(maximalni.userid==0)
                    return null;

                //if(maximalni.userid==31)

                return maximalni;
            } ).collect(Collectors.toList());



            double prosek = collect.stream()
                    .filter(unos -> unos !=null && unos.userid !=0  ) //unos.userid
                    .mapToLong(unos->unos.trajanje)
                    .average()
                    .getAsDouble();

            double ukupno = collect.stream()
                    .filter(unos -> unos !=null && unos.userid !=0  ) //unos.userid
                    .mapToLong(unos->unos.trajanje)
                    .sum();


            rez = new double[2];
            rez[0] = prosek;
            rez[1] = ukupno;

            return  rez;
            //System.out.println();
            //System.out.println("Prosecna sesija za sve user-e : " + prosek );

        } catch ( Exception e) {
            e.printStackTrace();
        }

        rez[0] = -1;
        return rez;

    }


    public static void ubaciLessonTime(Zakljucivanje zakljucivanje, double vreme, double ukupnaSesija, OntModel miniModel, String user) {

        long donjaGranica = Math.round( 0.5 *ukupnaSesija) ;
        long gornjaGranica =Math.round( 0.75 * ukupnaSesija ) ;

        if(miniModel == null)
            miniModel = zakljucivanje.ontModel;

        String nazivKlase = Zakljucivanje.putanja  + "#LessonTime";
        String nazivPropertija = Zakljucivanje.putanja  + "#timeSpentL";
        String tempIndividual = Zakljucivanje.putanja  + "#ltin"+user;

        String donjaGranicaNaziv = Zakljucivanje.putanja  + "#lowerBound";
        String gornjaGranicaNaziv = Zakljucivanje.putanja  + "#upperBound";



        OntClass nazivKl= miniModel.getOntClass(  nazivKlase );
        Individual indNov = miniModel.createIndividual(   tempIndividual , nazivKl );


        DatatypeProperty provedenoVreme = miniModel.createDatatypeProperty(  nazivPropertija  );
        Literal provedenoVremeVrednost = miniModel.createTypedLiteral( Math.round(vreme) ); //+0.0
        indNov.setPropertyValue( provedenoVreme , provedenoVremeVrednost );


        DatatypeProperty propertyWNumber = miniModel.createDatatypeProperty(  donjaGranicaNaziv  );
        Literal propertyNumberValue = miniModel.createTypedLiteral( donjaGranica ); //+0.0
        indNov.setPropertyValue( propertyWNumber , propertyNumberValue );

        DatatypeProperty propertyWNumber1 = miniModel.createDatatypeProperty(  gornjaGranicaNaziv  );
        Literal propertyNumberValue1 = miniModel.createTypedLiteral( gornjaGranica ); //+0.0
        indNov.setPropertyValue( propertyWNumber1 , propertyNumberValue1 );



        ObjectProperty relatedToUser = miniModel.createObjectProperty(Zakljucivanje.putanja + "#relatedToUser");
        ObjectProperty objectProperty1 = miniModel.createObjectProperty(  "http://www.mau.rs/resource/user/"+user);
        indNov.setPropertyValue(relatedToUser,objectProperty1);
       // zakljucivanje.insertIndividualWithoutStyle(koeficijent ,  miniModel, user, nazivKlase, donjaGranicaNaziv, tempIndividual, "");

    }

    public static int vremeProvedenoQuiz(Zakljucivanje zakljucivanje, String user, String courseId) {

        //"?s mau:logsCourse  ?c . "+
        //"?c mau:hasId "+courseId+" . "+

        String sparqlQueryString =
                Zakljucivanje.prefixi +
                        "SELECT ( sum(?time)   as ?broj) WHERE { "+
                        "  ?s ?p mau:QuizAttempts . "+
                        "?s mau:relatedToQuiz ?q . " +
                        "?q mau:relatedToCourse ?c . " +
                        "?c mau:hasId "+courseId+" . "+
                        "?s mau:timeSpent ?time. "+
                        "?s mau:relatedToUser	<http://www.mau.rs:2020/resource/user/"+user+">  . "+
                        "} "+
                        "LIMIT 10";

        Literal literal = zakljucivanje.izvrsiBrojUpit(sparqlQueryString);

        int anInt = 0;
        if(literal!=null)
            anInt = literal.getInt();
        return anInt;

    }

    public static void ubaciBorderValues(Zakljucivanje zakljucivanje, OntModel miniModel, int brojResursa, String courseId, String resurs) {


        long donjaGranica = Math.round(brojResursa *0.25);
        long gornjaGranica = Math.round(brojResursa *0.75);

        if(miniModel == null)
            miniModel = zakljucivanje.ontModel;

        String nazivKlase = Zakljucivanje.putanja  + "#BorderValues";
        String nazivPropertija = Zakljucivanje.putanja  + "#lowerBound";
        String nazivPropertija1 = Zakljucivanje.putanja  + "#upperBound";
        String relatRes = Zakljucivanje.putanja  + "#relatedToRes";
        String tempIndividual = Zakljucivanje.putanja  + "#bv"+courseId+"_"+resurs;

        OntClass borderVal = miniModel.getOntClass(  nazivKlase );
        Individual indNov = miniModel.createIndividual(   tempIndividual , borderVal );

        DatatypeProperty propertyWNumber = miniModel.createDatatypeProperty(  nazivPropertija  );
        Literal propertyNumberValue = miniModel.createTypedLiteral( donjaGranica ); //+0.0
        indNov.setPropertyValue( propertyWNumber , propertyNumberValue );

        DatatypeProperty propertyWNumber1 = miniModel.createDatatypeProperty(  nazivPropertija1  );
        Literal propertyNumberValue1 = miniModel.createTypedLiteral( gornjaGranica ); //+0.0
        indNov.setPropertyValue( propertyWNumber1 , propertyNumberValue1 );

        DatatypeProperty relatedToResource = miniModel.createDatatypeProperty(  relatRes  );
        Literal stringResource = miniModel.createTypedLiteral( resurs ); //+0.0
        indNov.setPropertyValue( relatedToResource, stringResource);

        ObjectProperty relatedToUser = miniModel.createObjectProperty(Zakljucivanje.putanja + "#relatedToCourse");
        ObjectProperty objectProperty1 = miniModel.createObjectProperty(  "http://www.mau.rs/resource/course/"+courseId);
        indNov.setPropertyValue(relatedToUser,objectProperty1);

    }


}
