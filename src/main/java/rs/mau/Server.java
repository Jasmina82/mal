package rs.mau;


import java.io.*;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.UriBuilder;
import org.glassfish.grizzly.Grizzly;
import org.glassfish.grizzly.http.server.ErrorPageGenerator;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;

public class Server {
    private static final Logger LOGGER = Grizzly.logger(Server.class);


    public static void main(String[] args) {
        LOGGER.setLevel(Level.ALL); //FINER



        //varovatno ce morati da se ubaci rs.mau.Zakljucivanje sa semantickim rezonovanjem

        URI uri = UriBuilder.fromUri("http://0.0.0.0/").port(7777).build();
        HttpServer server = GrizzlyHttpServerFactory.createHttpServer(uri,
                new ApplicationConfig(), false);

        ErrorPageGenerator epg = new ErrorPageGenerator(){

            public String generate(Request request, int status, String
                    reasonPhrase,
                                   String description,
                                   Throwable exception) {
                StringBuilder sb = new StringBuilder();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                PrintStream ps = new PrintStream(baos);
                exception.printStackTrace(ps);
                ps.close();
                sb.append(new String(baos.toByteArray()));
                System.out.println(sb.toString());
                return sb.toString();
            }
        };
        server.getServerConfiguration().setDefaultErrorPageGenerator(epg);
        /*try {
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        /*GrizzlyWebContainerFactory.create(uri,
                Collections.singletonMap("javax.ws.rs.Application",
                        "rs.mau.ApplicationConfig"));*/
        try {
            server.start();
            LOGGER.info("Press 's' to shutdown now the server...");
            while(true){
                int c = System.in.read();
                if (c == 's')
                    break;
            }
        } catch (IOException ioe) {
            LOGGER.log(Level.SEVERE, ioe.toString(), ioe);
        } finally {
            server.stop();
            LOGGER.info("rs.mau.Server stopped");
        }
    }
}
