package rs.mau;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

import java.util.List;

public class Misc {


    public static void listajSveIndividuale(OntModel ontModel, OntModel mod) {

        if(mod == null)
            mod = ontModel;

        ExtendedIterator<Individual> allIndividuals = mod.listIndividuals();
        while (allIndividuals.hasNext()){
            System.out.println( allIndividuals.next() );
        }
    }

    public static boolean cloneIndividualToNewModel(Individual individualTemp, OntModel tempModel) {

        if(individualTemp == null)
            return  false;

        tempModel.add(individualTemp.listProperties());
        return true;
    }

    public static void stampajPropertijeIndividuala(Individual individual) {
        StmtIterator stmtIterator = individual.listProperties();

        //System.out.println("size of list :" + stmtIterator.toList().size());

        List<Statement> statements = stmtIterator.toList();

        for (final Statement statement : statements) {
            System.out.println(statement);
        }
        //System.out.println("posle for-a");
    }
}
