package rs.mau;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import org.eclipse.persistence.jaxb.rs.MOXyJsonProvider;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;

import java.util.List;

public class ApplicationConfig extends ResourceConfig {



    /**
     * Main constructor
     */
    public ApplicationConfig() {

        register(MauWebService.class);
        register(MOXyJsonProvider.class);
        register(LogAllExceptions.class);
        register(CORSResponseFilter.class);
        /*register(new AbstractBinder() {

            @Override
            protected void configure() {
                bind(toDoList).to(ToDoList.class);
            }});*/
    }

}
