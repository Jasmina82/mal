package rs.mau;

/**
 *
 */
public class Unos {

    public long id;
    public long userid;
    public long timecreated;
    public long timeStart;
    public long timeEnd;
    public long trajanje;
    public String action;

    @Override
    public String toString() {
        return String.format(" Id : %d, user: %d, trajanje : %d sekundi , timecreated : %d  ",id, userid, trajanje, timecreated);
    }
}
